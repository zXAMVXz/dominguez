import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Data from '../data/projects.json';


@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnInit {

  projectData:any = Data;
  project:any
  product_id: any;
  constructor(private _router:Router) { }

  ngOnInit(): void {


    console.log(this.projectData);
    
  }


  goToProject(id:string){

    //para ir a la ruta del portfolio o detalle del proyecto
    console.log(id);
    this._router.navigate([`project/${id}`]);
    debugger;

  }

}
