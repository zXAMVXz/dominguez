import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import Data from '../data/projects.json';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.css']
})
export class ProjectDetailComponent implements OnInit {

  projectData:any[] = Data;
  project:any
  product_id: any;

  foundProject:any;
  constructor(private _route: ActivatedRoute) { }

  ngOnInit(): void {
    
    //recibiendo parametro de la ruta con el id del proyecto a buscar
    let { id } = this._route.snapshot.params;

    //buscando en la data el objeto con el proyecto
    this.foundProject = this.projectData.find(element => element.id == id ); 
    
    console.log(this.foundProject)



  }

}
