import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {


  items:any = [];
  constructor() { }

  ngOnInit(): void {

    this.items = [
      {
        label:'Sobre mí',
        id:'#aboutme',
      },
      {
        label:'Experiencia',
        id:'#work'
      },
      {
        label:'Proyectos',
        id:'#projects'
      },
      {
        label:'Cursos / Certificaciones',
        id:'#certification'
      },
      {
        label:'Contacto',
        id:'#contact'
      },

    ]


  }

  showMenu(){
    $('.sidenav').sidenav();
  }

}
