import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Data from '../data/projects.json';


// import Swiper core and required modules
import SwiperCore, { Navigation } from "swiper";

// install Swiper modules
SwiperCore.use([Navigation]);

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  projectData:any = Data;
  constructor(private _router:Router) {

  }

  onSwiper([swiper]) {
    console.log(swiper);
  }
  onSlideChange() {
    console.log('slide change');
  }

  ngOnInit(): void {
    
    console.log(this.projectData);
  }


  goToProject(id:string){

    //para ir a la ruta del portfolio o detalle del proyecto
    console.log(id);
    (id == 'all')? this._router.navigate(['portfolio']) : this._router.navigate([`project/${id}`]);

  }

}
